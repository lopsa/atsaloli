# Aleksey LOPSA TODO list

## Secretary

- update/consolidate procedural documentation pertaining to handling of board Minutes


- draft "thank you" letters for donations/sponsorships, as well as end-of-year Annual Statement / "Thank you for your support" letter.

- Update "committees" page on Governance web site and on lopsa.org (consolidate them?) also see below

- Update http://governance.lopsa.org/LOPSA_Policies/Committees to fix broken links:
- remove "Detail pages" link (requires board approval)
- add missing pages:
  - https://board.lopsa.org/Leadership_Committee
  - https://board.lopsa.org/Communications_Committee
  - https://board.lopsa.org/Marketing (needs to be updated)
  - https://board.lopsa.org/Membership_Committee (needs to be updated)
  - https://board.lopsa.org/LOPSA_Policies/Finance_Committee (needs to be updated)
  - https://board.lopsa.org/Recognition_Committee (needs to be updated)
  - https://board.lopsa.org/Conference_Committee (needs to be updated)
  - https://board.lopsa.org/Locals_Committee (needs to be updated)
  - https://board.lopsa.org/Technical_Services_Committee (needs to be updated)
  - https://board.lopsa.org/Education_%26_Training_Committee (needs to be updated)


## Leadership Committee

### Meet with Scott Murphy re cultivating volunteers and leadership

"In the past, LOPSA has had difficulty using
volunteers effectively; helping to change that is a Board role. It is
very important that the new organization get away from the Board serving
as the implementors for all programs." -- Board Candidate Infopack

We need volunteers to help with:
- Sponsorship (inactive except Treasurer is updating our list of Sponsors)
- Recognition (inactive)
- Communications/Marketing


## Marketing / Communications


- ?

## Chapters

- ask UUASC to update http://www.uuasc.org/la/ to remove the dead link to SAGE and to add a link to LOPSA

- Introduce Roger to the LOPSA Chicago chapter leaders

- Ask LOPSA chapter leaders for feedback on https://gitlab.com/lopsa/chapters/blob/master/chapter_handbook.md
http://governance.lopsa.org/LOPSA_Policies/Committees is out of date with regard to chairs and liaisons.
I move we update this policy to remove the chairs/liaisons columns.
(https://lopsa.org/Committees/ lists the committee memberships)
And the "Detail List" link leads to a bunch of junk...
